<?php
/**
 * Author: Lirrums
 * Programming Language: PHP
 * Topic: PHP + OOP + AJAX + JQUERY
 * Gitlab: https://gitlab.com/linacastrodev
*/
namespace Text;

Class Account{
    public $titular;
    public $cantidad;
    public $con;

    function __construct($cantidad,$titular)
    {
        $this->cantidad = $cantidad;
        $this->titular = $titular;
    }

    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;
    }

    public function getCantidad()
    {
        return $this->cantidad;
    }

    public function setTitular($titular)
    {
        $this->titular = $titular;
    }

    public function getTitular(){
        return $this->titular;
    }

    public function ingresar($cantidad)
    {
        if($cantidad > 0 )
        {
            $GLOBALS['con'] = 0;
            return 'Actuamente usted tiene en cuenta: '.'$'.$GLOBALS['con'] += $cantidad;
        }    
        else
        {
            return "debes ingresar una cantidad real";
        }

    }
    public function retirar($cantidad)
    { 
        if($cantidad <= 0 )
        {
            return "cuenta en 0, ingrese dinero";
        }
        elseif($cantidad >= 0 )
        {
            return 'Actualmente usted acaba de retirar: '.'$'.$cantidad.' desde su cuenta, '.'<br>'.'usted tiene '.'$'.$GLOBALS['con'] -= $cantidad;   
        }
    }   
}
?>