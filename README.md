# PHP OOP EXERCISES!

_This project is a website where people who show the deposit and draw about your account in the bank. This proyect made around the year 2020 when PHP begin with OOP (this proyect is create with OOP, PHP AND COMPOSER)

## Installation and initialization 🔧

_If you have already cloned the project code, you must install:_

```
COMPOSER 
```

_To start the project in developer mode use the script:_

```
COMPOSER INSTALL
```

## Built with 🛠️

- [PHP](https://www.php.net/)
- [COMPOSER](https://getcomposer.org/)
- [HTML5](https://developer.mozilla.org/en-US/docs/Web/Guide/HTML/HTML5)
- [CSS3](https://developer.mozilla.org/en-US/docs/Archive/CSS3) 

## Authors ✒️

- **Lina Castro** - _Frontend and backend Developer_ - [lirrums](https://gitlab.com/linacastrodev)

## License 📄

This project is under the License (MIT)

## Acknowledgements 🎁

- We greatly appreciate to the company for motivating us with the project. providing us with tools and their knowledge for our professional growth📢
