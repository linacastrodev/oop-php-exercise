<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width,initial-scale=1">
  <meta property="og:title" content="Algorithms OOP + PHP + COMPOSER" />
  <meta property="og:type" content="video.movie" />
  <meta property="og:url" content="http://lirrums.com.es/" />
  <meta property="og:image" content="https://upload.wikimedia.org/wikipedia/commons/thumb/2/27/PHP-logo.svg/1280px-PHP-logo.svg.png" />
  <title>Algorithms OOP + PHP</title>
  <link rel="stylesheet" type="text/css" href="src/assets/main.css">
  <link rel="icon" type="image/png" href="https://upload.wikimedia.org/wikipedia/commons/thumb/2/27/PHP-logo.svg/1280px-PHP-logo.svg.png">
</head>
<body>
    <h1>Welcome this little proyect <br> development by: <a href="https://twitter.com/lirrums" target="_BLANK" class="link">@Lirrums.</a></h1>
    <p>With technologies like: PHP + OOP, Composer, HTML5, CSS3 </p>
</body>
</html>
<?php
/**
 * Author: Lirrums
 * Programming Language: PHP
 * Topic: PHP + OOP + AJAX + JQUERY
 * Gitlab: https://gitlab.com/linacastrodev
*/

require __DIR__.'/vendor/autoload.php';
echo '<div class="container">';
echo '<span class="deposit">'.ingresar(30).'</span>';
echo '<br>';
echo '<span class="draw">'.retirar(5).'</span>';
echo '</div>';
?>